$(function () {
  const accountPopupOverlay = $('#account_popup_overlay');
  const accountPopupContainer = $('#account_popup_container');
  const accountPopupHeading = $('#account_popup_heading');
  const accountPopupContent = $('#account_popup_content');
  const accountPopupLoader = $('#account_popup_loader');
  const accountPopupCloseBtn = $('#account_popup_close');
  const headerAccountIcon = $('#header_account_icon');

  const loginForm = $('#customer_login_form');
  const customerLoginCreateAccountBtn = $('#customer_login_create_account');
  const customerLoginRecoverPasswordBtn = $('#customer_login_recover_password');
  const customerLoginErrors = $('#customer_login_errors');

  const registerForm = $('#create_customer_form');
  const createCustomerBackToLoginBtn = $('#create_customer_back_to_login');
  const createCustomerErrors = $('#create_customer_errors');

  const recoverPasswordForm = $('#recover_password_form');
  const recoverPasswordBackToLoginBtn = $('#recover_password_back_to_login');
  const recoverPasswordErrors = $('#recover_password_errors');

  $(document).ready(() => {
    //Login form submit
    loginForm.submit(e => {
      e.preventDefault();
      customerLoginErrors.empty();
      const email = $('#customer_login_email').val();
      const password = $('#customer_login_password').val();

      login(email, password).then(response => {
        const errors = $(response).find('.errors');

        if (errors.length > 0) {
          customerLoginErrors.html(errors.html());
        } else {
          location.reload();
        }
      });
    });

    //Register form submit
    registerForm.submit(e => {
      e.preventDefault();
      createCustomerErrors.empty();
      const firstName = $('#create_customer_first_name').val();
      const lastName = $('#create_customer_last_name').val();
      const email = $('#create_customer_email').val();
      const password = $('#create_customer_password').val();

      register(email, password, firstName, lastName).then(response => {
        const errors = $(response).find('.errors');

        if (errors.length > 0) {
          createCustomerErrors.html(errors.html());
        } else {
          location.reload();
        }
      });
    });

    //Recover password form submit
    recoverPasswordForm.submit(e => {
      e.preventDefault();
      recoverPasswordErrors.empty();
      const email = $('#recover_password_email').val();

      recoverPassword(email).then(response => {
        const errors = $(response).find('.errors');

        if (errors.length > 0) {
          recoverPasswordErrors.html(errors.html());
        }
      });
    });

    headerAccountIcon.add(accountPopupCloseBtn).on('click', (e) => {
      e.preventDefault();
      accountPopupOverlay.add(accountPopupContainer).toggleClass('active');
      accountPopupContent.addClass('show-login');
      accountPopupContent.removeClass('show-register');
      accountPopupContent.removeClass('show-recover');
      customerLoginErrors.empty();
      createCustomerErrors.empty();
      recoverPasswordErrors.empty();
    });

    customerLoginCreateAccountBtn.on('click', () => {
      accountPopupContent.addClass('show-register');
      accountPopupContent.removeClass('show-login');
      accountPopupHeading.html('create account');
      customerLoginErrors.empty();
    });

    customerLoginRecoverPasswordBtn.on('click', () => {
      accountPopupContent.addClass('show-recover');
      accountPopupContent.removeClass('show-login');
      accountPopupHeading.html('reset password');
      customerLoginErrors.empty();
    });

    createCustomerBackToLoginBtn.add(recoverPasswordBackToLoginBtn).on('click', () => {
      accountPopupContent.addClass('show-login');
      accountPopupContent.removeClass('show-register');
      accountPopupContent.removeClass('show-recover');
      accountPopupHeading.html('login');
      createCustomerErrors.empty();
      recoverPasswordErrors.empty();
    });
  });

  function login(email, password) {
    const data = {
      'customer[email]': email,
      'customer[password]': password,
      form_type: 'customer_login',
      utf8: 'U+2713'
    };

    return ajax('/account/login', data);
  }

  function register(email, password, firstName, lastName) {
    const data = {
      'customer[email]': email,
      'customer[password]': password,
      'customer[first_name]': firstName,
      'customer[last_name]': lastName,
      form_type: 'create_customer',
      utf8: 'U+2713'
    };

    return ajax('/account', data);
  }

  function recoverPassword(email) {
    const data = {
      'email': email,
      form_type: 'recover_customer_password',
      utf8: 'U+2713'
    };

    return ajax('/account/recover', data);
  }

  function ajax(url, data) {
    return $.ajax({
      url: url,
      method: 'post',
      data: data,
      dataType: 'html',
      async: true,

      beforeSend: () => {
        accountPopupLoader.addClass('active');
      },
      complete: () => {
        accountPopupLoader.removeClass('active');
      }
    });
  }
});
