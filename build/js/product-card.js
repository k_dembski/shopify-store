class ProductCard extends HTMLElement {
  constructor() {
    super();
    this.container = $(this).find('#product-card_container');
    this.info = $(this).find('#product-card_info');
    this.infoContainer = $(this).find('#product-card_info_container');
    this.details = $(this).find('#product-card_details');
    this.variantRadios = $(this).find('input[type=radio][name=variant]');
    this.variantIdInput = $(this).find('input[type=hidden][name=id]');

    this.init();
  }

  init() {
    $(document).ready(() => {
      $(this.container).css('padding-bottom', $(this.info).height() + 'px').hover(() => {
        $(this.infoContainer).css('transform', 'translateY(0)');
      }, () => {
        $(this.infoContainer).css('transform', 'translateY(' + $(this.details).height() + 'px)');
      });
      $(this.infoContainer).css('transform', 'translateY(' + $(this.details).height() + 'px)');
      $(this.variantRadios).on('change', (e) => {
        this.variantIdInput.val(e.target.value);
      });
    });
  };
}

customElements.define('product-card', ProductCard);
